<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package weaa
 */

get_header(); ?>

	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>


						<div class="team-detail-thumb">
						<img src="<?php the_field('team_member_portrait'); ?>"/>
						</div>

					<div class="team-detail-excerpt">
					<h2><?php the_title(); ?></h2>
					<h3><?php the_field('team_member_title'); ?></h3>

<!--- BOARD & STAFF MEMBER FIELDS -->				
<!-- FIRST -->
<?php if(get_field('team_member_phone')) {?>	
	<p style="font-size: .85em;"><?php the_field('team_member_phone'); ?></p>
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('team_member_phone')) {?>	
	
<?php }?> <!-- ends the second outer condition -->
<!-- SECOND -->
<?php if(get_field('team_member_email')) {?>	
	<p style="font-size: .85em;"><a href="mailto:<?php the_field('team_member_email'); ?>?subject=Mail from Our Site"><?php the_field('team_member_email'); ?></a></p>
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('team_member_email')) {?>	
	
<?php }?> <!-- ends the second outer condition -->

<!--THIRD -->
<?php if(get_field('team_member_history')) {?>
<div class="team-detail-clear"></div>
<h4>Education:</h4>
<p><?php the_field('team_member_history'); ?></p>

<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('team_member_history')) {?>	
	
<?php }?> <!-- ends the second outer condition -->
			
<!--FOURTH -->
<?php if(get_field('team_member_education')) {?>
<div class="team-detail-clear"></div>
<h4>Career summary:</h4>
<p><?php the_field('team_member_education'); ?></p>

<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('team_member_education')) {?>	
	
<?php }?> <!-- ends the second outer condition -->		

<!--FIFTH -->
<?php if(get_field('team_member_insight')) {?>
<div class="team-detail-clear"></div>
<h4>Community/volunteer commitments:</h4>
<p><?php the_field('team_member_insight'); ?></p>

<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('team_member_insight')) {?>	
	
<?php }?> <!-- ends the second outer condition -->	

<!--SIXTH-->
<?php if(get_field('team_member_interests')) {?>
<div class="team-detail-clear"></div>
<h4>Interests:</h4>
<p><?php the_field('team_member_interests'); ?></p>

<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('team_member_interests')) {?>	
	
<?php }?> <!-- ends the second outer condition -->	

<!-- SCHOLAR FIELDS -->
<!--FIRST-->
<?php if(get_field('scholar_class_of')) {?>
<div class="team-detail-clear"></div>
<h4>Class of: <span style="color:#282828; font-weight:normal; font-size:.85em;"><?php the_field('scholar_class_of'); ?></span></h4>
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('scholar_class_of')) {?>	

<?php }?> <!-- ends the second outer condition -->	

<!--SECOND -->
<?php if(get_field('scholar_home_town')) {?>
<div class="team-detail-clear"></div>
<h4>Hometown: <span style="color:#282828; font-weight:normal;font-size:.85em;"><?php the_field('scholar_home_town'); ?></span> </h4>
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('scholar_home_town')) {?>	
	
<?php }?> <!-- ends the second outer condition -->	

<!--THIRD -->
<?php if(get_field('scholar_major')) {?>
<div class="team-detail-clear"></div>
<h4>Major: <span style="color:#282828; font-weight:normal; font-size:.85em;"><?php the_field('scholar_major'); ?></span></h4>
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('scholar_major')) {?>	
	
<?php }?> <!-- ends the second outer condition -->

<!--FOURTH -->
<?php if(get_field('scholar_why_northwestern')) {?>
<div class="team-detail-clear"></div>
<h4>Why I Chose Northwestern: </h4>
<p><?php the_field('scholar_why_northwestern'); ?></p>

<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('scholar_why_northwestern')) {?>	
	
<?php }?> <!-- ends the second outer condition -->	

<!--FIFTH -->
<?php if(get_field('scholar_activities')) {?>
<div class="team-detail-clear"></div>
<h4>Campus Activities and organizations:  </h4>
<p><?php the_field('scholar_activities'); ?></p>

<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('scholar_activities')) {?>	
	
<?php }?> <!-- ends the second outer condition -->	

<!--SIXTH -->
<?php if(get_field('scholar_post_graduation_plans')) {?>
<div class="team-detail-clear"></div>
<h4>What I plan to do after graduation:   </h4>
<p><?php the_field('scholar_post_graduation_plans'); ?></p>

<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('scholar_post_graduation_plans')) {?>	
	
<?php }?> <!-- ends the second outer condition -->	

<!--SEVENTH -->
<?php if(get_field('scholar_weaa_difference')) {?>
<div class="team-detail-clear"></div>
<h4>How being a WEAA Scholar has made a difference in my undergraduate experience:   </h4>
<p><?php the_field('scholar_weaa_difference'); ?></p>

<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('scholar_weaa_difference')) {?>	
	
<?php }?> <!-- ends the second outer condition -->	

					</div><!-- ends archive excerpt -->
			
			<div class="team-detail"><?php the_content(); ?></div>
		


		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
