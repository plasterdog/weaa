<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package weaa
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>

		<?php if(get_field('title_icon')) {?>	
			<?php if(get_field('alternate_title_text')) {?>	
			<div class="icon-title-section">
			<div class="icon-section-image" style="background-color:<?php the_field('title_icon_background'); ?>;">
				<img src="<?php the_field('title_icon'); ?> "/>
			</div><!-- ends title section image -->
			<div class="icon-section-title">
				<h1 class="entry-title"><?php the_field('alternate_title_text'); ?></h1>
			</div><!-- ends icon section title -->
			</div><!-- ends title-section-->	
			<?php }	?>

			<?php if(!get_field('alternate_title_text')) {?>	
			<div class="icon-title-section">
			<div class="icon-section-image" style="background-color:<?php the_field('title_icon_background'); ?>;">
				<img src="<?php the_field('title_icon'); ?> "/>
			</div><!-- ends title section image -->
			<div class="icon-section-title">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</div><!-- ends icon section title -->
			</div><!-- ends title-section-->	
			<?php }?>
			<div class="icon-content-section">	<?php the_content(); ?>		</div><!--ends clear -->
		<?php } ?><!-- ends the first outer condition -->

		<?php if(!get_field('title_icon')) {?>	
			<?php if(get_field('alternate_title_text')) {?>	
			<h1 class="entry-title"><?php the_field('alternate_title_text'); ?></h1>
			<?php }	?>
			<?php if(!get_field('alternate_title_text')) {?>	
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php }?>
			<?php the_content(); ?>
		<?php }?> <!-- ends the second outer condition -->


		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->
		<aside id="secondary" class="widget-area" role="complementary">
     <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
    <?php endif; // end sidebar widget area ?>
    </aside><!-- #secondary -->

	</div><!-- #primary -->

<?php

get_footer();
