<?php
/**
 * Template Name:Leadership Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package weaa
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="entry-content">

	

			<?php if(get_field('alternate_title_text')) {?>	
			<div class="icon-title-section">
			
			<h1 class="entry-title"><?php the_field('alternate_title_text'); ?></h1>
			
			</div><!-- ends title-section-->	
			<?php }	?>

			<?php if(!get_field('alternate_title_text')) {?>	
			<div class="icon-title-section">
			
			<h1 class="entry-title"><?php the_title(); ?></h1>
			
			</div><!-- ends title-section-->	
			
	<?php } ?><!-- ends the first outer condition -->		
	


			
	
			
			<div class="clear" style="padding:0;"><?php the_content(); ?></div><!-- .entry-content -->
			<hr/>
			
		                <ul class="short-bios">
                    <?php
                    // check if the repeater field has rows of data
                    if( have_rows('leadership_short_bios') ): ?>                  

                    <?php while ( have_rows('leadership_short_bios') ) : the_row(); ?>

                    <li>
                   
                    <div class="clear">
                   
                    	<?php if( get_sub_field('leadership_image_size_toggle') == 'left' ): ?>

                    			
                    		<div class="short-bio-image">

                    		<?php if( get_sub_field('leadership_link') ) { ?>
                    		<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_sub_field('leadership_link'); ?> ">
                    		<img src="<?php the_sub_field('leadership_headshot'); ?>"/></a>
                    		 <?php } ?>

                    		 <?php if(!get_sub_field('leadership_link') ) { ?>
                    		 	<img src="<?php the_sub_field('leadership_headshot'); ?>"/>
                    		  <?php } ?>	
                    		</div><!-- ends short bio image -->
    					
                    		<div class="short-bio-content">
                    		<?php if( get_sub_field('leadership_link') ) { ?>	
                    		<h2><a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_sub_field('leadership_link'); ?> "><?php echo the_sub_field('leadership_name'); ?></a></h2>
                    		<?php } ?>

                    		<?php if(!get_sub_field('leadership_link') ) { ?>
                    			<h2><?php echo the_sub_field('leadership_name'); ?></h2>
                    		<?php } ?>	

                    		<p><?php echo the_sub_field('leadership_title'); ?></p>
                    		<p><?php echo the_sub_field('leadership_phone'); ?></p>
                    		<?php if ( get_sub_field( 'leadership_email_address' ) ): ?>
                    		<p><a href="mailto:<?php the_sub_field('leadership_email_address'); ?> "><?php echo the_sub_field('leadership_email_address'); ?></a></p>  
                    		<p class="leader-left-link"><a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_sub_field('leadership_link'); ?> ">Full bio</a></p>
                    		<?php else: // field_name returned false ?>	
							&nbsp;
							<?php if( get_sub_field('leadership_link') ) { ?>
							<p class="leader-left-link"><a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_sub_field('leadership_link'); ?> ">Full bio</a></p>
							<?php } ?>

							<?php if(!get_sub_field('leadership_link') ) { ?>

							<?php } ?>
							<?php endif; // end of if field_name logic ?>                 	 
							<div class="short-bio-blurb"><?php echo the_sub_field('leadership_short_statement'); ?></div>
							</div><!-- ends short bio content -->
						


						<?php endif; ?>

                    	<?php if( get_sub_field('leadership_image_size_toggle') == 'right' ): ?>
                    		<div class="short-bio-image-right">

                    		<?php if( get_sub_field('leadership_link') ) { ?>	
                    		<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_sub_field('leadership_link'); ?> ">
                    		<img src="<?php the_sub_field('leadership_headshot'); ?>"/></a> 
                    		<?php } ?>

                    		<?php if(!get_sub_field('leadership_link') ) { ?>
                    		<img src="<?php the_sub_field('leadership_headshot'); ?>"/>
                    		<?php } ?>	

                    		</div><!-- ends short bio image -->
    					
                    		<div class="short-bio-content-right">

                    		<?php if( get_sub_field('leadership_link') ) { ?>		
                    		<h2><a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_sub_field('leadership_link'); ?> "><?php echo the_sub_field('leadership_name'); ?></a></h2>
                    		<?php } ?>	

                    		<?php if(!get_sub_field('leadership_link') ) { ?>
                    		<h2><?php echo the_sub_field('leadership_name'); ?></h2>
                    		<?php } ?>	

                    		<p><?php echo the_sub_field('leadership_title'); ?></p>
                    		<p><?php echo the_sub_field('leadership_phone'); ?></p>
                    		<?php if ( get_sub_field( 'leadership_email_address' ) ): ?>
                    		<p><a href="mailto:<?php the_sub_field('leadership_email_address'); ?> "><?php echo the_sub_field('leadership_email_address'); ?></a></p> 
                    		<p class="leader-right-link"> <a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_sub_field('leadership_link'); ?> ">Full bio</a></p>
                    		<?php else: // field_name returned false ?>	
							&nbsp;

							<?php if( get_sub_field('leadership_link') ) { ?>
							<p class="leader-left-link"><a href="<?php echo esc_url( home_url( '/' ) ); ?><?php the_sub_field('leadership_link'); ?> ">Full bio</a></p>
							<?php } ?>

							<?php if(!get_sub_field('leadership_link') ) { ?>

							<?php } ?>

							<?php endif; // end of if field_name logic ?>                 	                	 
							<div class="short-bio-blurb"><?php echo the_sub_field('leadership_short_statement'); ?></div>
							</div><!-- ends short bio content -->
						<?php endif; ?>



			</div><!-- ends clear container -->
<hr/>



                    	                                     
                    </li>                                                   	                
	                 <?php
	                 endwhile; ?>
	                 <?php else : ?>
	                <?php  // no rows found
	                 endif;
	                 ?> 
	                 </ul>

	

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'weaa' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->

			
<?php 
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

	








<?php
get_footer();
