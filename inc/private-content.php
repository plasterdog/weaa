<?php
//JMC allow subscriber to see private posts => http://stackoverflow.com/questions/11407181/wordpress-private-posts
$subrole = get_role( 'subscriber' );
// For private pages
$subrole->add_cap( 'read_private_pages' );
/// For private posts
$subrole->add_cap( 'read_private_posts' );

// JMC show admin bar only for admins and editors => https://digwp.com/2011/04/admin-bar-tricks/#disable-for-non-admins
if (!current_user_can('edit_posts')) {
  add_filter('show_admin_bar', '__return_false');
}
//JMC removes the "private" prefix => http://www.trickspanda.com/2014/03/remove-privateprotected-wordpress-post-titles/
function title_format($content) {
return '%s';
}
add_filter('private_title_format', 'title_format');
add_filter('protected_title_format', 'title_format');

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
	// add your extension to the array
	$existing_mimes['vcf'] = 'text/x-vcard';
	return $existing_mimes;
}