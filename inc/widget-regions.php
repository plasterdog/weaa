<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function weaa_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'weaa' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'this is the right column sidebar', 'weaa' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Case Study Sidebar', 'weaa' ),
		'id'            => 'casestudy',
		'description'   => esc_html__( 'this is a unique sidebar for case study related pages, posts & archives', 'weaa' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'weaa_widgets_init' );
