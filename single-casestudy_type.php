<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package weaa
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>


<div class="casestudy-example">
<h1><?php the_title(); ?></h1>

<?php if(get_field('casestudy_category')) {?>	
	<h3>Category: <span style="font-weight:normal;"><?php the_field('casestudy_category'); ?></span></h3>
	
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('casestudy_category')) {?>	
<?php }?> <!-- ends the second outer condition -->

<?php if(get_field('client_challenge')) {?>	
	<h3>Challenge:</h3>
	<?php the_field('client_challenge'); ?>
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('client_challenge')) {?>	
<?php }?> <!-- ends the second outer condition -->

<?php if(get_field('pk_solution')) {?>	
	<h3>Solution:</h3>
	<?php the_field('pk_solution'); ?>
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('pk_solution')) {?>	
<?php }?> <!-- ends the second outer condition -->

<?php if(get_field('pk_solution')) {?>	
	<h3>Final Thoughts:</h3>
	<?php the_field('pk_solution'); ?>
<?php } ?><!-- ends the first condition -->		
<?php if(!get_field('pk_solution')) {?>	
<?php }?> <!-- ends the second outer condition -->

						
</div><!--ends case study example -->		


		<?php endwhile; // End of the loop.
		?>

		</main><!-- #main -->
		
<aside id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'casestudy' ); ?>
</aside><!-- #secondary -->




	</div><!-- #primary -->

<?php
get_footer();
