<?php
/**
 * WEAA functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package weaa
 */

require_once( __DIR__ . '/inc/backend-experience.php');
require_once( __DIR__ . '/inc/widget-styling.php');
require_once( __DIR__ . '/inc/widget-regions.php');
require_once( __DIR__ . '/inc/private-content.php');

if ( ! function_exists( 'weaa_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function weaa_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on weaa, use a find and replace
	 * to change 'weaa' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'weaa', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'weaa' ),
		'footer' => esc_html__( 'Footer', 'weaa' ),
		'department_foot' => esc_html__( 'Department Footer', 'weaa' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'weaa_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'weaa_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function weaa_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'weaa_content_width', 640 );
}
add_action( 'after_setup_theme', 'weaa_content_width', 0 );


/**
 * Enqueue scripts and styles.
 */
function weaa_scripts() {
	wp_enqueue_style( 'weaa-style', get_stylesheet_uri() );

	wp_enqueue_style('plasterdog-flexslider', get_template_directory_uri() . '/flexslider.css');

	wp_enqueue_script( 'weaa-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'weaa-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'plasterdog-flexslider-min', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array('jquery'), '20120206', true );

 	wp_enqueue_script( 'plasterdog-flexslider-one', get_template_directory_uri() . '/js/flexslider-one.js', array('plasterdog-flexslider-min'), '20120206', true );
  
	wp_enqueue_script( 'plasterdog-flexslider-two', get_template_directory_uri() . '/js/flexslider-two.js', array('plasterdog-flexslider-min'), '20120206', true );	


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'weaa_scripts' );

/*--- JMC/LOADING OPEN SANS FONT ---*/
function wpb_add_google_fonts() {
 
wp_enqueue_style( 'wpb-google-fonts', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300', false ); 
}
 
add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


 /* JMC --* Enable support for Post Thumbnails	 */
	add_theme_support( 'post-thumbnails' );


 //JMC remove anchor link from more tag

function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');


//JMC https://wordpress.org/support/topic/remove-category-from-title/
add_filter( 'get_the_archive_title', 'remove_category_word_from_archive_title' );

function remove_category_word_from_archive_title( $title ) {
    $title = str_replace( 'Category:', '', $title );
    return $title;
}

add_filter('the_content', 'remove_empty_p', 20, 1);
function remove_empty_p($content){
    $content = force_balance_tags($content);
    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}