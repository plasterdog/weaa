<?php
/**
 * Template Name:Split Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package weaa
 */

get_header(); ?>

	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="entry-content">

	
			<?php if(get_field('alternate_title_text')) {?>	
			<div class="icon-title-section">

				<h1 class="entry-title"><?php the_field('alternate_title_text'); ?></h1>
			
			</div><!-- ends title-section-->	
			<?php }	?>

			<?php if(!get_field('alternate_title_text')) {?>	
			<div class="icon-title-section">
			
				<h1 class="entry-title"><?php the_title(); ?></h1>
			
			</div><!-- ends title-section-->	
			<?php }?>
				<?php the_content(); ?>		
			<?php if(get_field('split_left_section')) {?>	
			<div class="left-side">
			<?php the_field('split_left_section'); ?>	
			</div><!-- ends left side-->
			<div class="right-side">
			<?php the_field('split_right_section'); ?>		
			</div><!-- ends left side-->			
			<?php }	?>
			<?php if(!get_field('split_left_section')) {?>
			<?php }	?>	
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'weaa' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->

			
<?php 
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->






<?php
get_footer();
