<?php
/**
 * The template for displaying case study archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package weaa
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
			<h1><?php echo str_replace("Cohort: ", "", get_the_archive_title()); ?></h1>
			<?php	the_archive_description( '<div class="archive-description">', '</div>' );
				?>
				<hr/>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
													
					<div class="industry-array">
						<div class="archive-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark">
						<a href="<?php the_permalink(); ?>" rel="bookmark">
						<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail('medium');
						} else { ?>
						<img src="<?php bloginfo('template_directory'); ?>/images/default-thumbnail.png" alt="<?php the_title(); ?>" />
						<?php } ?>
						</a></div>
						<div class="archive-excerpt"><h1><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
						<h3>Challenge:</h3>
						<?php the_field('client_challenge'); ?>
						</div>




					<div class="clear">
							<footer class="entry-footer"></footer><!-- .entry-footer -->
					<hr/></div>
					
</article><!-- #post-## -->
			
			<?php 
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
		<aside id="secondary" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'casestudy' ); ?>
</aside><!-- #secondary -->
	</div><!-- #primary -->

<?php
get_footer();
