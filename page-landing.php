<?php
/**
 *Template Name:Slider Landing Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package weaa
 */

get_header('landing'); ?>
<div class="outer-slider-container">
<div class="slider-container">
		<div id="slider" class="flexslider">         
                <ul class="slides">
                    <?php
                    // check if the repeater field has rows of data
                    if( have_rows('top_slider_repeater') ): ?>                  

                    <?php while ( have_rows('top_slider_repeater') ) : the_row(); ?>

                    <li><a href="<?php the_sub_field('top_slider_link_target'); ?> ">
                    <img src="<?php the_sub_field('top_slider_image'); ?>"/></a>
                    <div class="flex-caption">    
                    <div class="flex-inner-caption">       
<a href="<?php the_sub_field('top_slider_link_target'); ?> ">
                    	<p><?php echo the_sub_field('top_slider_caption_text'); ?></p>
                    	
                    	<?php if( get_sub_field('top_slider_link_text') ):?>	
                    	<?php echo the_sub_field('top_slider_link_text'); ?>
                    	<?php endif; ?>
</a>
</div><!-- ends flex inner caption -->
                    </div><!-- ends flex caption -->
                    </li>
	                 <?php
	                 endwhile; ?>
	                 <?php else : ?>
	                <?php  // no rows found
	                 endif;
	                 ?> 
	                 </ul>
	        </div><!--- ends flexslider -->

</div><!-- ends slider container-->	 

</div><!-- ends clear -->

<div id="content" class="site-content">
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="entry-content">

	
<div class="front-left-region">
		
		<div class="front-left-content"><?php the_field('content_left_side'); ?></div>
</div>
<div class="front-right-region">
		<h3 class="front-sidebar-title"><?php the_field('right_side_title'); ?></h3>
		<div class="front-right-content"><?php the_field('content_right_side'); ?></div>
</div>

</div><!-- ends slider container-->	 





	</div><!-- .entry-content -->







	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'weaa' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->

			
<?php 
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
