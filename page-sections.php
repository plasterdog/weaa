<?php
/**
 *Template Name:Section Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package weaa
 */

get_header(); ?>

	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="entry-content">

	<h1 class="entry-title"><?php the_title(); ?></h1>

	<?php the_content(); ?>
	
<ul class="sections">
                    <?php
                    // check if the repeater field has rows of data
                    if( have_rows('section_repeater') ): ?>                  
                    <?php while ( have_rows('section_repeater') ) : the_row(); ?>
                    <li>			
			<div class="icon-title-section">

			<div class="icon-section-title">
				<h3><?php the_sub_field('section_title_text'); ?></h3>
				<?php the_sub_field('section_content_region'); ?>
			</div><!-- ends icon section title -->

			<div class="icon-section-image">
				<img src="<?php the_sub_field('section_title_icon'); ?> "/>
						<?php if( get_sub_field('section_second_image') ): ?>
			<img src="<?php the_sub_field('section_second_image'); ?>"/>
		<?php endif; ?>

			</div><!-- ends title section image -->
			
			</div><!-- ends title-section-->	
	
                    </li>
	                 <?php
	                 endwhile; ?>
	                 <?php else : ?>
	                <?php  // no rows found
	                 endif; ?> 
 </ul>


	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'weaa' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->

			
<?php 
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
	