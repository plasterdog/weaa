<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package weaa
 */

get_header(); ?>

	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header" style="margin:0 5%">
			<h1 class="entry-title"><?php echo str_replace("Department: ", "", get_the_archive_title()); ?></h1>
			<?php	the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>
			<li class="team-member-array">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if( get_field('allow_link_to_bio') == 'yes' ): ?>
					<a href="<?php the_permalink(); ?>" rel="bookmark"> <img src="<?php the_field('team_member_portrait'); ?>"/></a>
					<?php endif; ?>
					<?php if( get_field('allow_link_to_bio') == 'no' ): ?>
					<img src="<?php the_field('team_member_portrait'); ?>"/>
					<?php endif; ?>
						
					<?php if( get_field('allow_link_to_bio') == 'yes' ): ?>
					<h2><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<?php endif; ?>
					<?php if( get_field('allow_link_to_bio') == 'no' ): ?>
					<h2><?php the_title(); ?></h2>
					<?php endif; ?>

					<h3><?php the_field('team_member_title'); ?></h3>
					<?php if ( get_field( 'team_member_phone' ) ): ?>
					<p><?php the_field('team_member_phone'); ?></p>
					<?php else: // field_name returned false ?>	
					
					<?php endif; // end of if field_name logic ?>

					<?php if ( get_field( 'team_member_email' ) ): ?>
					<p><a href="mailto:<?php the_field('team_member_email'); ?>?subject=Mail from Our Site"><?php the_field('team_member_email'); ?></a></p>
					<?php else: // field_name returned false ?>	
					
					<?php endif; // end of if field_name logic ?>

					<div class="clear">
					<footer class="entry-footer"></footer><!-- .entry-footer -->
					<hr/></div>
				</article><!-- #post-## -->
			</li>
			<?php 
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
<div class="clear"></div>
		</main><!-- #main -->
	</div><!-- #primary -->


	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="site-info">
			<div class="centering-foot">
<div class="footer-menu"><?php wp_nav_menu( array( 'theme_location' => 'department_foot' ) ); ?></div><!-- ends footer menu -->
				
	<?php if(get_option('pdog_linklabel') && get_option('pdog_linklabel') != '') {?>
	<div class="footer-focus-link">	
	<a href="<?php echo get_option('pdog_linktarget') ?>" target="_blank"><?php echo get_option('pdog_linklabel') ?></a>
	</div>
	<?php } ?>
		
		
				<div class="social">
				<ul class="foot-social-icons">
	

					<?php if(get_option('pdog_facebook') && get_option('pdog_facebook') != '') {?>
					<li><a href="<?php echo get_option('pdog_facebook') ?>" target="_blank"><i class="fa fa-facebook"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_linkedin') && get_option('pdog_linkedin') != '') {?>
					<li><a href="<?php echo get_option('pdog_linkedin') ?>" target="_blank"><i class="fa fa-linkedin"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_googleplus') && get_option('pdog_googleplus') != '') {?>
					<li><a href="<?php echo get_option('pdog_googleplus') ?>" target="_blank"><i class="fa fa-google-plus"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_twitter') && get_option('pdog_twitter') != '') {?>
					<li><a href="<?php echo get_option('pdog_twitter') ?>" target="_blank"><i class="fa fa-twitter"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_instagram') && get_option('pdog_instagram') != '') {?>
					<li><a href="<?php echo get_option('pdog_instagram') ?>" target="_blank"><i class="fa fa-instagram"></i></a>	</li><?php } ?>	

										
					<?php if(get_option('pdog_youtube') && get_option('pdog_youtube') != '') {?>
					<li><a href="<?php echo get_option('pdog_youtube') ?>" target="_blank"><i class="fa fa-youtube-square"></i></a>	</li><?php } ?>	
			
					<?php if(get_option('pdog_vimeo') && get_option('pdog_vimeo') != '') {?>
					<li><a href="<?php echo get_option('pdog_vimeo') ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a>	</li><?php } ?>										
				
					<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
					<li><a href="mailto:<?php echo get_option('pdog_email') ?>?subject=Website enquiry" ><i class="fa fa-envelope-o"></i></a>	</li><?php } ?>	
										
				</ul>
				</div><!-- ends social-->
				<span class="footer-copyright">&copy;&nbsp;
			
			<?php if(get_option('pdog_sitename') && get_option('pdog_sitename') != '') {?>
			 <?php echo get_option('pdog_sitename') ?>
			 <?php } ?>
					&nbsp;<?php $the_year = date("Y"); echo $the_year; ?>
			<br/>
			<?php if(get_option('pdog_address') && get_option('pdog_address') != '') {?>
			 <?php echo get_option('pdog_address') ?>
			<?php } ?>
			<?php if(get_option('pdog_phone2') && get_option('pdog_phone2') != '') {?>
			| <?php echo get_option('pdog_phone2') ?>
			<?php } ?>
			</span>	

		</div><!-- ends clear -->
			<div class="left-foot">
				<div class="logo-left-foot">

				</div><!-- ends logo-left-foot -->
				<div class="left-foot-contact">

				</div><!-- ends left-foot-contact -->
			</div> <!-- ends left foot -->

			<div class="right-foot">
			


			</div><!-- ends right foot -->

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
