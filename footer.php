<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package weaa
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="site-info">
			<div class="centering-foot">
<div class="footer-menu"><?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?></div><!-- ends footer menu -->
				
	<?php if(get_option('pdog_linklabel') && get_option('pdog_linklabel') != '') {?>
	<div class="footer-focus-link">	
	<a href="<?php echo get_option('pdog_linktarget') ?>" target="_blank"><?php echo get_option('pdog_linklabel') ?></a>
	</div>
	<?php } ?>
		
		
				<div class="social">
				<ul class="foot-social-icons">
	

					<?php if(get_option('pdog_facebook') && get_option('pdog_facebook') != '') {?>
					<li><a href="<?php echo get_option('pdog_facebook') ?>" target="_blank"><i class="fa fa-facebook"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_linkedin') && get_option('pdog_linkedin') != '') {?>
					<li><a href="<?php echo get_option('pdog_linkedin') ?>" target="_blank"><i class="fa fa-linkedin"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_googleplus') && get_option('pdog_googleplus') != '') {?>
					<li><a href="<?php echo get_option('pdog_googleplus') ?>" target="_blank"><i class="fa fa-google-plus"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_twitter') && get_option('pdog_twitter') != '') {?>
					<li><a href="<?php echo get_option('pdog_twitter') ?>" target="_blank"><i class="fa fa-twitter"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_instagram') && get_option('pdog_instagram') != '') {?>
					<li><a href="<?php echo get_option('pdog_instagram') ?>" target="_blank"><i class="fa fa-instagram"></i></a>	</li><?php } ?>	

										
					<?php if(get_option('pdog_youtube') && get_option('pdog_youtube') != '') {?>
					<li><a href="<?php echo get_option('pdog_youtube') ?>" target="_blank"><i class="fa fa-youtube-square"></i></a>	</li><?php } ?>	
			
					<?php if(get_option('pdog_vimeo') && get_option('pdog_vimeo') != '') {?>
					<li><a href="<?php echo get_option('pdog_vimeo') ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a>	</li><?php } ?>										
				
					<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
					<li><a href="mailto:<?php echo get_option('pdog_email') ?>?subject=Website enquiry" ><i class="fa fa-envelope-o"></i></a>	</li><?php } ?>	
										
				</ul>
				</div><!-- ends social-->
				<span class="footer-copyright">&copy;&nbsp;
			
			<?php if(get_option('pdog_sitename') && get_option('pdog_sitename') != '') {?>
			 <?php echo get_option('pdog_sitename') ?>
			 <?php } ?>
					&nbsp;<?php $the_year = date("Y"); echo $the_year; ?>
			<br/>
			<?php if(get_option('pdog_address') && get_option('pdog_address') != '') {?>
			 <?php echo get_option('pdog_address') ?>
			<?php } ?>
			<?php if(get_option('pdog_phone2') && get_option('pdog_phone2') != '') {?>
			| <?php echo get_option('pdog_phone2') ?>
			<?php } ?>
			</span>	

		</div><!-- ends clear -->
			<div class="left-foot">
				<div class="logo-left-foot">

				</div><!-- ends logo-left-foot -->
				<div class="left-foot-contact">

				</div><!-- ends left-foot-contact -->
			</div> <!-- ends left foot -->

			<div class="right-foot">
			


			</div><!-- ends right foot -->

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
